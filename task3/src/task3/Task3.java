package task3;

public class Task3 {

	
	public static void main(String[] args) {
		
		int [] array = {3, 2, 2, 1, 2, 3, 1, 5};

//find Min
		int min = array[0];
		int indexOfMin = 0;
		indexOfMin = 0;
		for (int i = 0; i < array.length; i++) {
			if (array[i] <= min) {
				min = array[i];
				indexOfMin = i;
				}	
		}
		System.out.println("Min = " + min + "; IndexOfMin = "+ indexOfMin);
//find Avg		
		int sum = 0;
		for (int i = 0; i < array.length; i ++) {
			sum = sum + array[i];
			}
		int avg = sum / array.length;
		System.out.println("Avg = " + avg);
//change Element
		for (int i = 0; i < array.length; i++) {
			if (i == indexOfMin) {
			array[i] = avg;
			}
		}
		printArray(array);
	}
	
		
	public static void printArray(int[] array) {
		System.out.print("[");
		int arrayNumbers = array.length;
		for (int i = 0; i < arrayNumbers; i++) {
			if (i == arrayNumbers - 1) {
				System.out.print(array[i]);
			} else {
				System.out.print(array[i] + ",");
			}
		}
		System.out.print("]");
	}		
}