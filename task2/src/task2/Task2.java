package task2;

import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

public class Task2 {

	public static void main(String[] args) {
		
		Integer [] numbers = {19, 12, 12, 10, 24, 25, 25};
		List<Integer> listNumbers = Arrays.asList(numbers);
		
		Collections.reverse(listNumbers);
		Set<Integer> withoutDupl = new LinkedHashSet <Integer> (listNumbers);
		
		Integer [] newNumbers = new Integer [withoutDupl.size()];
		newNumbers = withoutDupl.toArray(newNumbers);
		
		System.out.println(Arrays.toString(newNumbers));
		
	}
}